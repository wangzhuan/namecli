package api

import (
	"bytes"
	"errors"
	"fmt"
	"net"
	"os"
	"strconv"
	"strings"
	"sync/atomic"
)

var (
	// agent addr
	Addr = "127.0.0.1:8328"
	seq  uint32
)

func init() {
	k8sNodeIP := os.Getenv("K8S_NODE_IP")
	k8sNodeIP = strings.TrimSpace(k8sNodeIP)

	if k8sNodeIP != "" {
		_, port, _ := net.SplitHostPort(Addr)
		Addr = k8sNodeIP + ":" + port
	}
}

func Name(name string) (addr string, err error) {
	retried := false
again:
	conn, err := net.Dial("udp", Addr)
	if err != nil {
		return
	}
	defer conn.Close()

	seq := atomic.AddUint32(&seq, 1)
	req := fmt.Sprintf("%d,%s", seq, name)
	_, err = conn.Write([]byte(req))
	if err != nil {
		return
	}

	rsp := [64]byte{}
	n, err := conn.Read(rsp[:])
	if err != nil || n <= 0 {
		return
	}

	_rsp := rsp[:n]
	i := bytes.IndexByte(_rsp, ',')
	if i == -1 {
		// no expected
		return
	}
	_seq, _ := strconv.ParseUint(string(_rsp[:i]), 10, 32)
	if seq != uint32(_seq) {
		if !retried {
			retried = true
			goto again
		}
		err = errors.New("seq invalid from namesrv")
		return
	}

	addr = string(_rsp[i+1:])
	if addr == "" {
		if !retried {
			retried = true
			goto again
		}
		err = fmt.Errorf("no addr found from namesrv: %s", name)
		return
	}

	return
}
