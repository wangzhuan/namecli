module gitlab.com/wangzhuan/namecli

go 1.12

require (
	gitlab.com/wangzhuan/lc v1.1.1
	gitlab.com/wangzhuan/utils v1.0.4
)
